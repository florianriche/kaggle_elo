# -*- coding: utf-8 -*-
import click
import os
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
from cleaning import train_test

log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)

logger = logging.getLogger(__name__)


@click.group()
def main():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger.info('making final data set from raw data')
    logger.info(os.environ.get("RAW_DIR"))


@main.command()
def prepare_train_test():
    logger.info("prepared_train_test_ is called!")
    train_file = "{}/train.csv".format(os.environ.get("RAW_DIR"))
    test_file = "{}/test.csv".format(os.environ.get("RAW_DIR"))
    train_test.clean_train_and_test(train_file, test_file)



if __name__ == '__main__':
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()
