def simple(data):
    df_train_columns = [c for c in data.columns if c not in ['first_active_month', 'target', 'card_id', 'outliers',
                                                              'hist_purchase_date_max', 'hist_purchase_date_min', 'hist_card_id_size',
                                                             'new_purchase_date_max', 'new_purchase_date_min', 'new_card_id_size']]
    y = data[["card_id"]]
    if "target" in data:
        y = data["target"]
        del data["target"]

    X = data[df_train_columns]
    return (X,y)
