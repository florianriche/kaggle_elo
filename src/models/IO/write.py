import os
from .utils import reduce_mem_usage


def write_submission(prediction, submission_name="submission.csv"):
    prediction = prediction[["card_id", "target"]]
    prediction.to_csv('{}/{}'.format(os.environ.get("PROCESSED_DIR"), submission_name), index=False)

