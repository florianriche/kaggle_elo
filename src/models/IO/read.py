import os
import pandas as pd
from .utils import reduce_mem_usage

def read_processed_train():
    train_file = "{}/train_dataset.csv".format(os.environ.get("PROCESSED_DIR"))
    train = pd.read_csv(train_file,parse_dates=["first_active_month"])
    return train

def read_processed_test():
    test_file = "{}/test_dataset.csv".format(os.environ.get("PROCESSED_DIR"))
    test = pd.read_csv(test_file,parse_dates=["first_active_month"])
    return test