from xgboost import XGBRegressor
import lightgbm as lgb


def xgb_model(X, Y):
    model = XGBRegressor(max_depth=7,
                         learning_rate=0.01,
                         n_estimators=100,
                         silent=True,
                         objective='reg:linear',
                         booster='gbtree',
                         n_jobs=1,
                         nthread=None,
                         gamma=0,
                         min_child_weight=41.9612869171337,
                         max_delta_step=0,
                         subsample=1,
                         colsample_bytree=0.5665320670155495,
                         colsample_bylevel=1,
                         reg_alpha=9.677537745007898,
                         reg_lambda=8.2532317400459,
                         scale_pos_weight=1,
                         base_score=0.5,
                         random_state=0,
                         seed=None,
                         missing=None,
                         importance_type='gain')
    model.fit(X, Y)
    return model


def lgb_model(X, Y):
    trn_data = lgb.Dataset(X, label=Y, free_raw_data=False)
    # params optimized by optuna
    param = {
        'task': 'train',
        'boosting': 'goss',
        'objective': 'regression',
        'metric': 'rmse',
        'learning_rate': 0.01,
        'subsample': 0.9855232997390695,
        'max_depth': 7,
        'top_rate': 0.9064148448434349,
        'num_leaves': 63,
        'min_child_weight': 41.9612869171337,
        'other_rate': 0.0721768246018207,
        'reg_alpha': 9.677537745007898,
        'colsample_bytree': 0.5665320670155495,
        'min_split_gain': 9.820197773625843,
        'reg_lambda': 8.2532317400459,
        'min_data_in_leaf': 21,
        'verbose': -1,
    }
    num_round = 10000
    clf_r = lgb.train(param,
                      trn_data,
                      num_round,
                      verbose_eval=-1,
                    #   early_stopping_rounds=200
                      )
    return clf_r