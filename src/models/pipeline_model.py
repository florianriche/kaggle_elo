# -*- coding: utf-8 -*-
import click
import pandas as pd
import os
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import IO
from column_selection import simple as simple_selection
from train_model import xgb_model, lgb_model
import pickle
import numpy as np
from sklearn.metrics import mean_squared_error

log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)

logger = logging.getLogger(__name__)


@click.group()
def main():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger.info('making final data set from raw data')
    logger.info(os.environ.get("RAW_DIR"))


@main.command()
def basic_xgb_pipeline():
    logger.info("prepared_train_test_ is called!")
    train = IO.read.read_processed_train()
    test = IO.read.read_processed_test()

    (X_train, Y_train) = simple_selection(train)
    (X_test, submission) = simple_selection(test)

    logging.info("Start training")
    clf = xgb_model(X_train, Y_train)

    logging.info("Done training, start prediction")
    if not os.path.isfile("first_xgb_model.dat"):
        pickle.dump(clf, open("first_xgb_model.dat", "wb"))
    
    logging.info("Train score: {}".format(np.sqrt(mean_squared_error(clf.predict(X_train), Y_train))))
    # clf = pickle.load(open("first_xgb_model.dat", "rb"))
    submission.loc[:, "target"] = clf.predict(X_test)
    IO.write.write_submission(submission)


@main.command()
def basic_lgb_pipeline():
    logger.info("prepared_train_test_ is called!")
    train = IO.read.read_processed_train()
    test = IO.read.read_processed_test()

    (X_train, Y_train) = simple_selection(train)
    (X_test, submission) = simple_selection(test)

    logging.info("Start training")
    clf = lgb_model(X_train, Y_train)

    logging.info("Done training, start prediction")
    if not os.path.isfile("first_lgb_model.dat"):
        pickle.dump(clf, open("first_lgb_model.dat", "wb"))
    
    logging.info("Train score:{}".format( np.sqrt(mean_squared_error(clf.predict(X_train), Y_train))))
    # clf = pickle.load(open("first_lgb_model.dat", "rb"))
    submission.loc[:, "target"] = clf.predict(X_test)
    IO.write.write_submission(submission, submission_name="lgb.csv")
    



if __name__ == '__main__':
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()
