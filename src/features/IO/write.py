import os
from .utils import reduce_mem_usage


def write_interim_train(train):
    train_file = "{}/train.csv".format(os.environ.get("INTERIM_DIR"))
    train.to_csv(train_file, index=False)

def write_interim_test(test):
    test_file = "{}/test.csv".format(os.environ.get("INTERIM_DIR"))
    test.to_csv(test_file, index=False)

def write_interim_historical_transactions(transactions):
    transactions.to_csv('{}/historical_transactions.csv'.format(os.environ.get("INTERIM_DIR")), index=False)

def write_interim_new_transactions(transactions):
    transactions.to_csv('{}/new_merchant_transactions.csv'.format(os.environ.get("INTERIM_DIR")), index=False)

def write_processed_train(train):
    train.to_csv('{}/train_dataset.csv'.format(os.environ.get("PROCESSED_DIR")), index=False)

def write_processed_test(test):
    test.to_csv('{}/test_dataset.csv'.format(os.environ.get("PROCESSED_DIR")), index=False)