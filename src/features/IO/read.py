import os
import pandas as pd
from .utils import reduce_mem_usage


# RAW STEPS
def read_raw_train():
    train_file = "{}/train.csv".format(os.environ.get("RAW_DIR"))
    train = reduce_mem_usage(pd.read_csv(train_file,parse_dates=["first_active_month"]))
    return train
    

def read_raw_test():
    test_file = "{}/test.csv".format(os.environ.get("RAW_DIR"))
    test = reduce_mem_usage(pd.read_csv(test_file,parse_dates=["first_active_month"]))
    return test
    

def read_raw_historical_transactions():
    transactions = reduce_mem_usage(pd.read_csv('{}/historical_transactions.csv'.format(os.environ.get("RAW_DIR"))))
    return transactions


def read_raw_new_transactions():
    transactions = reduce_mem_usage(pd.read_csv('{}/new_merchant_transactions.csv'.format(os.environ.get("RAW_DIR"))))
    return transactions


# PROCESSED STEPS
def read_interim_train():
    train_file = "{}/train.csv".format(os.environ.get("INTERIM_DIR"))
    train = reduce_mem_usage(pd.read_csv(train_file,parse_dates=["first_active_month"]))
    return train
    

def read_interim_test():
    test_file = "{}/test.csv".format(os.environ.get("INTERIM_DIR"))
    test = reduce_mem_usage(pd.read_csv(test_file,parse_dates=["first_active_month"]))
    return test
    

def read_interim_historical_transactions():
    transactions = reduce_mem_usage(pd.read_csv('{}/historical_transactions.csv'.format(os.environ.get("INTERIM_DIR"))))
    return transactions


def read_interim_new_transactions():
    transactions = reduce_mem_usage(pd.read_csv('{}/new_merchant_transactions.csv'.format(os.environ.get("INTERIM_DIR"))))
    return transactions
