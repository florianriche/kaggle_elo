
def combine_features(transactions):
    transactions['duration'] = transactions['purchase_amount']*transactions['month_diff']
    transactions['amount_month_ratio'] = transactions['purchase_amount']/transactions['month_diff']
    transactions['price'] = transactions['purchase_amount'] / transactions['installments']
    return transactions
