
def combine_features(new_transactions):
    new_transactions['duration'] = new_transactions['purchase_amount']*new_transactions['month_diff']
    new_transactions['amount_month_ratio'] = new_transactions['purchase_amount']/new_transactions['month_diff']
    new_transactions['price'] = new_transactions['purchase_amount'] / new_transactions['installments']
    return new_transactions
