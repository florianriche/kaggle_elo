import numpy as np
def clean_transactions(transactions):
    #impute missing values - This is now excluded.
    transactions['category_2'] = transactions['category_2'].fillna(1.0,inplace=True)
    transactions['category_3'] = transactions['category_3'].fillna('A',inplace=True)
    transactions['merchant_id'] = transactions['merchant_id'].fillna('M_ID_00a6ca8a8a',inplace=True)
    transactions['installments'].replace(-1, np.nan,inplace=True)
    transactions['installments'].replace(999, np.nan,inplace=True)
    transactions['purchase_amount'] = transactions['purchase_amount'].apply(lambda x: min(x, 0.8))

    #Feature Engineering - Adding new features inspired by Chau's first kernel
    transactions['authorized_flag'] = transactions['authorized_flag'].map({'Y': 1, 'N': 0})
    transactions['category_1'] = transactions['category_1'].map({'Y': 1, 'N': 0})
    transactions['category_3'] = transactions['category_3'].map({'A':0, 'B':1, 'C':2})
    return transactions