
import datetime
import pandas as pd
import numpy as np
import logging

def remove_outliers(train, test):
    train['outliers'] = 0
    train.loc[train['target'] < -30, 'outliers'] = 1

    for features in ['feature_1','feature_2','feature_3']:
        order_label = train.groupby([features])['outliers'].mean()
        train[features] = train[features].map(order_label)
        test[features] =  test[features].map(order_label)
    return (train, test)

def date_extractions(data):
    data['days'] = (datetime.date(2018, 2, 1) - data['first_active_month'].dt.date).dt.days
    data['quarter'] = data['first_active_month'].dt.quarter

    feature_cols = ['feature_1', 'feature_2', 'feature_3']
    for f in feature_cols:
        data['days_' + f] = data['days'] * data[f]
        data['days_' + f + '_ratio'] = data[f] / data['days']
    return data


def clean_train_and_test(train, test):
    train, test = remove_outliers(train, test)
    train = date_extractions(train)
    test = date_extractions(test)
    return (train, test)
    

