import numpy as np
def clean_transactions(new_transactions):
    #impute missing values - This is now excluded.
    new_transactions['category_2'] = new_transactions['category_2'].fillna(1.0,inplace=True)
    new_transactions['category_3'] = new_transactions['category_3'].fillna('A',inplace=True)
    new_transactions['merchant_id'] = new_transactions['merchant_id'].fillna('M_ID_00a6ca8a8a',inplace=True)
    new_transactions['installments'].replace(-1, np.nan,inplace=True)
    new_transactions['installments'].replace(999, np.nan,inplace=True)
    new_transactions['purchase_amount'] = new_transactions['purchase_amount'].apply(lambda x: min(x, 0.8))

    #Feature Engineering - Adding new features inspired by Chau's first kernel
    new_transactions['authorized_flag'] = new_transactions['authorized_flag'].map({'Y': 1, 'N': 0})
    new_transactions['category_1'] = new_transactions['category_1'].map({'Y': 1, 'N': 0})
    new_transactions['category_3'] = new_transactions['category_3'].map({'A':0, 'B':1, 'C':2}) 
    return new_transactions