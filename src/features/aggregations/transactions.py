import pandas as pd

def mean_agg(transactions):
    agg_func = {
            'mean': ['mean'],
        }
    for col in ['category_2','category_3']:
        transactions[col+'_mean'] = transactions['purchase_amount'].groupby(transactions[col]).agg('mean')
        transactions[col+'_max'] = transactions['purchase_amount'].groupby(transactions[col]).agg('max')
        transactions[col+'_min'] = transactions['purchase_amount'].groupby(transactions[col]).agg('min')
        transactions[col+'_sum'] = transactions['purchase_amount'].groupby(transactions[col]).agg('sum')
        agg_func[col+'_mean'] = ['mean']
    return transactions

def aggregate_transaction_hist(trans, prefix):  
        
    agg_func = {
        'purchase_amount' : ['sum','max','min','mean','var','skew'],
        'installments' : ['sum','max','mean','var','skew'],
        'purchase_date' : ['max','min'],
        'month_lag' : ['max','min','mean','var','skew'],
        'month_diff' : ['max','min','mean','var','skew'],
        'weekend' : ['sum', 'mean'],
        'weekday' : ['sum', 'mean'],
        'authorized_flag': ['sum', 'mean'],
        'category_1': ['sum','mean', 'max','min'],
        'card_id' : ['size','count'],
        'month': ['nunique', 'mean', 'min', 'max'],
        'hour': ['nunique', 'mean', 'min', 'max'],
        'weekofyear': ['nunique', 'mean', 'min', 'max'],
        'day': ['nunique', 'mean', 'min', 'max'],
        'subsector_id': ['nunique'],
        'merchant_category_id' : ['nunique'],
        'price' :['sum','mean','max','min','var'],
        'duration' : ['mean','min','max','var','skew'],
        'amount_month_ratio':['mean','min','max','var','skew']
        
    }
    
    agg_trans = trans.groupby(['card_id']).agg(agg_func)
    agg_trans.columns = [prefix + '_'.join(col).strip() 
                           for col in agg_trans.columns.values]
    agg_trans.reset_index(inplace=True)
    
    df = (trans.groupby('card_id')
          .size()
          .reset_index(name='{}transactions_count'.format(prefix)))
    
    agg_trans = pd.merge(df, agg_trans, on='card_id', how='left')
    
    return agg_trans

