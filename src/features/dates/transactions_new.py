import pandas as pd
import datetime

def extract_date_features(new_transactions):
 
    new_transactions['purchase_date'] = pd.to_datetime(new_transactions['purchase_date'])
    new_transactions['month'] = new_transactions['purchase_date'].dt.month
    new_transactions['weekofyear'] = new_transactions['purchase_date'].dt.weekofyear
    new_transactions['day'] = new_transactions['purchase_date'].dt.day
    new_transactions['weekday'] = new_transactions.purchase_date.dt.weekday
    new_transactions['weekend'] = (new_transactions.purchase_date.dt.weekday >=5).astype(int)
    new_transactions['hour'] = new_transactions['purchase_date'].dt.hour 
    new_transactions['month_diff'] = ((datetime.datetime.today() - new_transactions['purchase_date']).dt.days)//30
    new_transactions['month_diff'] += new_transactions['month_lag']
    return new_transactions