import pandas as pd
import datetime

def extract_date_features(transactions):
    transactions['purchase_date'] = pd.to_datetime(transactions['purchase_date'])
    transactions['weekofyear'] = transactions['purchase_date'].dt.weekofyear
    transactions['month'] = transactions['purchase_date'].dt.month
    transactions['day'] = transactions['purchase_date'].dt.day
    transactions['weekday'] = transactions.purchase_date.dt.weekday
    transactions['weekend'] = (transactions.purchase_date.dt.weekday >=5).astype(int)
    transactions['hour'] = transactions['purchase_date'].dt.hour 
    transactions['month_diff'] = ((datetime.datetime.today() - transactions['purchase_date']).dt.days)//30
    transactions['month_diff'] += transactions['month_lag']
    return transactions