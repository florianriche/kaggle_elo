import pandas as pd
import datetime
import numpy as np

def final_features_historical(data):
    #Feature Engineering - Adding new features inspired by Chau's first kernel
    data['hist_purchase_date_max'] = pd.to_datetime(data['hist_purchase_date_max'])
    data['hist_purchase_date_min'] = pd.to_datetime(data['hist_purchase_date_min'])
    data['hist_purchase_date_diff'] = (data['hist_purchase_date_max'] - data['hist_purchase_date_min']).dt.days
    data['hist_purchase_date_average'] = data['hist_purchase_date_diff']/data['hist_card_id_size']
    data['hist_purchase_date_uptonow'] = (datetime.datetime.today() - data['hist_purchase_date_max']).dt.days
    data['hist_purchase_date_uptomin'] = (datetime.datetime.today() - data['hist_purchase_date_min']).dt.days
    data['hist_first_buy'] = (data['hist_purchase_date_min'] - data['first_active_month']).dt.days
    data['hist_last_buy'] = (data['hist_purchase_date_max'] - data['first_active_month']).dt.days

    for feature in ['hist_purchase_date_max','hist_purchase_date_min']:
        data[feature] = data[feature].astype(np.int64) * 1e-9
    
    return data


def final_features_new(data):
    #Feature Engineering - Adding new features inspired by Chau's first kernel
    data['new_purchase_date_max'] = pd.to_datetime(data['new_purchase_date_max'])
    data['new_purchase_date_min'] = pd.to_datetime(data['new_purchase_date_min'])
    data['new_purchase_date_diff'] = (data['new_purchase_date_max'] - data['new_purchase_date_min']).dt.days
    data['new_purchase_date_average'] = data['new_purchase_date_diff']/data['new_card_id_size']
    data['new_purchase_date_uptonow'] = (datetime.datetime.today() - data['new_purchase_date_max']).dt.days
    data['new_purchase_date_uptomin'] = (datetime.datetime.today() - data['new_purchase_date_min']).dt.days
    data['new_first_buy'] = (data['new_purchase_date_min'] - data['first_active_month']).dt.days
    data['new_last_buy'] = (data['new_purchase_date_max'] - data['first_active_month']).dt.days
    for feature in ['new_purchase_date_max','new_purchase_date_min']:
        data[feature] = data[feature].astype(np.int64) * 1e-9
    return data