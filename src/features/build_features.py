# -*- coding: utf-8 -*-
import click
import pandas as pd
import os
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import cleaning
import combinations
import dates
import aggregations
import IO

log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)

logger = logging.getLogger(__name__)


@click.group()
def main():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger.info('making final data set from raw data')
    logger.info(os.environ.get("RAW_DIR"))


@main.command()
def prepare_train_test():
    logger.info("prepared_train_test_ is called!")
    train = IO.read.read_raw_train()
    test = IO.read.read_raw_test()
    # import pdb; pdb.set_trace()
    train, test = cleaning.train_test.clean_train_and_test(train, test)
    logging.info("Done preparing train_test")

    IO.write.write_interim_train(train)
    IO.write.write_interim_test(test)



@main.command()
def prepare_transactions():
    logger.info("Prepare historical transaction processing")
    logging.info("Read")
    transactions = IO.read.read_raw_historical_transactions()
    logging.info("Clear data")
    transactions = cleaning.transactions.clean_transactions(transactions)
    logging.info("Extract date featues")
    transactions = dates.transactions.extract_date_features(transactions)
    logging.info("combine features")
    transactions = combinations.transactions.combine_features(transactions)
    logging.info("Aggregation features")
    transactions = aggregations.transactions.mean_agg(transactions)
    transactions = aggregations.transactions.aggregate_transaction_hist(transactions, prefix="hist_")
    # import pdb; pdb.set_trace()
    logging.info("Done processing transations")

    IO.write.write_interim_historical_transactions(transactions)


@main.command()
def prepare_transactions_new():
    
    logging.info("Read")
    transactions_new = IO.read.read_raw_new_transactions()
    logging.info("Clear data")
    transactions_new = cleaning.transactions_new.clean_transactions(transactions_new)
    logging.info("Extract date featues")
    transactions_new = dates.transactions_new.extract_date_features(transactions_new)
    logging.info("combine features")
    transactions_new = combinations.transactions_new.combine_features(transactions_new)
    logging.info("Aggregation features")
    transactions_new = aggregations.transactions_new.mean_agg(transactions_new)
    transactions_new = aggregations.transactions_new.aggregate_transaction_hist(transactions_new, prefix="new_")
    # import pdb; pdb.set_trace()
    logging.info("Done processing transations")

    IO.write.write_interim_new_transactions(transactions_new)


@main.command()
def merge_final_dataset():
        
    train = IO.read.read_interim_train()
    test = IO.read.read_interim_test()
    transactions = IO.read.read_interim_historical_transactions()
    transactions_new = IO.read.read_interim_new_transactions()

    train = pd.merge(train, transactions, on='card_id',how='left')
    test = pd.merge(test, transactions, on='card_id',how='left')

    train = dates.final_dataset.final_features_historical(train)
    test = dates.final_dataset.final_features_historical(test)

    train = pd.merge(train, transactions_new, on='card_id',how='left')
    test = pd.merge(test, transactions_new, on='card_id',how='left')

    train = dates.final_dataset.final_features_new(train)
    test = dates.final_dataset.final_features_new(test)

    train = combinations.final_dataset.combine_final_features(train)
    test = combinations.final_dataset.combine_final_features(test)

    IO.write.write_processed_train(train)
    IO.write.write_processed_test(test)
    



if __name__ == '__main__':
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()
